;;;; Simple Test Framework
;;;; a simple test framework for Scheme programs
;;;; 
;;;; Sample Usage
;;;; ------------
;;;; (define (add x y) (+ x y))
;;;; (define add-tests
;;;;   (test-suite add
;;;;               "tests for the add function"
;;;;               (list
;;;;                (test-case arith-equal '(2 3) 5 "add positive integers")
;;;;                (test-case arith-equal '(1 -2) -1 "add positive and negative integers")
;;;;                (test-case arith-not-equal '(3 3) 7 "check positive integers")
;;;;                (test-case arith-equal '(-2 -3) -5 "add negative integers"))))
;;;; (run-test-suite add-tests) =>
;;;; '((#t test-case arith-equal '(-2 -3) -5 "add negative integers"))
;;;;   (#t test-case arith-not-equal '(3 3) 7 "check positive integers")
;;;;   (#t test-case arith-equal '(1 -2) -1 "add positive and negative integers")
;;;;   (#t test-case arith-equal '(2 3) 5 "add positive integers"))
;;;;
;;;; (define (buggy-add x y) (+ (abs x) (abs y)))
;;;; (define buggy-and-tests
;;;;   (test-suite buggy-add
;;;;               "tests for buggy-add"
;;;;               (list
;;;;                (test-case arith-equal '(2 3) 5 "add positive integers")
;;;;                (test-case arith-equal '(1 -2) -1 "add positive and negative integers")
;;;;                (test-case arith-not-equal '(3 3) 7 "check positive integers")
;;;;                (test-case arith-equal '(-2 -3) -5 "add negative integers"))))
;;;; (run-test-suite buggy-add-tests) =>
;;;; '((#f test-case arith-equal '(-2 -3) -5 "add negative integers")
;;;;   (#t test-case arith-not-equal '(3 3) 7 "check positive integers")
;;;;   (#f test-case arith-equal '(1 -2) -1 "add positive and negative integers")
;;;;   (#t test-case arith-equal '(2 3) 5 "add positive integers"))


;; Conviniences
(define nil '())

;; Comparison Functions
;; Arbitrary binary functions can be defined and used
;; The following are just builtin for convinience
;;
(define arith-equal =)
(define arith-not-equal
  (lambda (a b)
    (not (= a b))))

;; Test Cases
;; Constructor
;; (test-case <comparison-function> <arg-list> <expression> <docstring>)
;; <comparison-function> -> binary boolean function
;; <expression> -> abitrary expression
;; <docstring> -> string
(define (test-case cmp-fn arg-list expected docstring)
  (list cmp-fn arg-list expected docstring))

;; test-case accessors
(define (get-test-case-docstring tc)
  "return the docstring of tc"
  (last tc))

(define (get-comparison-function tc)
  "return the comparison function of tc"
  (first tc))

(define (get-arglist tc)
  "return the argument list of tc"
  (second tc))

(define (get-expected tc)
  "return the expected result of tc"
  (third tc))

;; test case executor
(define (run-test-case fn tc)
  "return #t iff calling fn with the arguments in arglist of tc is equal to
the expected result as compared by comparison-function"
  (let ((cmp (get-comparison-function tc))
        (args (get-arglist tc))
        (expected (get-expected tc)))
    (cmp expected (apply fn args))))

;; Test Suites
;; Constructor
(define (test-suite fn docstring test-list)
  (list fn docstring test-list))

;; Accessors
(define (get-test-case-list ts)
  "return the list of test cases in ts"
  (last ts))

(define (get-test-suite-docsctring ts)
  "return the test-suite's docstring"
  (second ts))

(define (get-test-suite-function ts)
  "return the subject function of the test suite ts"
  (first ts))

;; test suite executor
(define run-test-suite
  (lambda (ts)
    "return a list with each element having the form: (bool test-case)"
    (let* ((f (get-test-suite-function ts))
           (bin-fun (lambda (acc tc)
                      (cons (cons (run-test-case f tc) tc) acc))))
      (fold-left bin-fun nil (get-test-case-list ts)))))

;;; Result Reporter
(define (pretty-print-test-suite-result ts)
  "pretty print the result of running the test suite ts.
The order of the test cases in ts is preserved."
  (pretty-print (cons (get-test-suite-docsctring ts)
                      (reverse (run-test-suite ts)))))


;;; Meta test:: testing the test frame work
(define (add x y) (+ x y))
(define (buggy-add x y) (+ (abs x) (abs y)))

(define add-test-cases
  (list
   (test-case arith-equal '(2 3) 5 "add positive integers")
   (test-case arith-equal '(1 -2) -1 "add positive and negative integers")
   (test-case arith-not-equal '(3 3) 7 "check positive integers")
   (test-case arith-equal '(-2 -3) -5 "add negative integers"))  )

(define add-tests
  (test-suite add "tests for the add function" add-test-cases))

(define buggy-add-tests
  (test-suite buggy-add "tests for buggy-add" add-test-cases))
